using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Remote;

namespace AspAutomation
{
    public class BaseTest
    {
        public IWebDriver Driver { get; set; }

        public ThePages UI => new ThePages(Driver);

        [OneTimeSetUp]
        public void SetUp() 
        {
            //EdgeOptions options = new();
            //options.AddArgument("--headless=new");
            
            //options.AddArgument("-inprivate");
            //options.AddArgument("--disable-notifications");
            //Driver = new EdgeDriver(options);
            ChromeOptions options = new ChromeOptions();
            
            options.AddArguments("--incognito");
            options.AddArgument("--headless"); // Run Chrome in headless mode for CI/CD
            options.AddArgument("--no-sandbox");
            options.AddArgument("--disable-gpu");

            //Driver = new ChromeDriver(options);

            Driver = new RemoteWebDriver(new Uri("http://selenium-hub:4444"), options);
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            //screenshot
            Driver.Quit();
        }
    }
}
