﻿using NUnit.Framework;
using NUnit.Framework.Legacy;

namespace AspAutomation.Features
{
    [TestFixture]
    public class FirstFeature : BaseTest
    {
        [Test]
        public void TitleTest()
        {
            UI.OpenUrl("https://www.google.com.ua/?hl=uk");
            var actualTitle = UI.SearchMainPage.GetTitleText();

            Assert.That("Google", Is.EqualTo(actualTitle));
        }

        [Test]
        [Ignore("To make run simpler and faster")]
        public void SearchTest()
        {
            UI.OpenUrl("https://www.google.com.ua/?hl=uk");

            var actualTitle = UI.SearchMainPage.GetTitleText();
            UI.SearchMainPage.DenyCookies();
            UI.SearchMainPage.TypeSearchCriteria("Who am I to disagree");
            UI.SearchMainPage.ClickLuckySearchButton();
            UI.SearchMainPage.DenySecondCookies();

            ClassicAssert.AreEqual("Google", actualTitle);
        }
    }
}