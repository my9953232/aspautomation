﻿using OpenQA.Selenium;

namespace AspAutomation.Pages
{
    public class SearchMainPage
    {
        private IWebDriver driver;
        private ThePages ui;

        public SearchMainPage(ThePages ui, IWebDriver driver)
        {
            this.ui = ui;
            this.driver = driver;
        }

        public string GetTitleText()
        {
            return driver.Title;
        }

        public void DenyCookies() 
        {
            ui.Click(By.XPath("//div[text()='Відхилити всі']"));
            //span[contains(text(), 'Reject all')]
        }
        public void DenySecondCookies()
        {
            ui.Click(By.XPath("//span[contains(text(), 'Reject all')]/../..//div[@class='yt-spec-touch-feedback-shape__fill']"));
        }

        public void TypeSearchCriteria(string text) 
        {
            ui.Type(By.XPath("//textarea[@role]"), text);
        }

        public void ClickLuckySearchButton()
        {
            ui.Click(By.XPath("//div[not(@jsname)]/center/input[@name='btnI' and @data-ved]"));
        }
    }
}
