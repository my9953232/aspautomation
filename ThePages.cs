﻿using AspAutomation.Pages;
using OpenQA.Selenium;

namespace AspAutomation
{
    public class ThePages
    {
        private IWebDriver driver;
        private SearchMainPage searchMainPage;

        public ThePages(IWebDriver driver)
        {
            this.driver = driver;
        }

        public SearchMainPage SearchMainPage => searchMainPage ?? new SearchMainPage(this, driver);

        public void Click(By selector) 
        {
            driver.FindElement(selector).Click();
        }

        public void Type(By selector, string text)
        {
            driver.FindElement(selector).SendKeys(text);
        }

        public void OpenUrl(string url)
        {
            driver.Navigate().GoToUrl(url);
        }
    }
}